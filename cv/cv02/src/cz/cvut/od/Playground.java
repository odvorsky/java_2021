package cz.cvut.od;

import cz.cvut.od.model.Animal;
import cz.cvut.od.model.Counter;
import cz.cvut.od.model.FuelType;
import cz.cvut.od.model.PrimitiveValues;

public class Playground {

    public void play() {
        System.out.println("PLAY CALLED");
        methodsAttributes();
        types();
        defaultPrimitiveValues();
        incDec();
        immutableString();
        compare();
        FuelType fuelType = FuelType.DIESEL;
    }

    public void compare(){
        int a = 1;
        int b = 1;
        if(a == b){
            System.out.println("int eq");
        }
        String as = "Jan";
        String ab = "Jin";
        ab = ab.replace("i", "a");
        if(as == ab){
            System.out.println("str ==");
        }
        if(as.equals(ab)){
            System.out.println("str equals");
        }
        ab = "Jan";
        if(as == ab){
            System.out.println("str ==");
        }
        if(as.equals(ab)){
            System.out.println("str equals");
        }



    }

    public void immutableString(){
        System.out.println("IMM STRING");
        String name = "Jan";
        name.replace("a", "i");
        System.out.println(name);
        name = name.replace("a", "i");
        System.out.println(name);

        String[] arr = {"A", "B", "C", "D", "E"};
        String result = "";
        for(String str: arr){
            result += str;
        }
        System.out.println(result);

        StringBuilder stringBuilder = new StringBuilder();
        for(String str: arr){
            stringBuilder.append(str);
        }
        System.out.println(stringBuilder.toString());


    }

    public void incDec(){
        int counter = 1;
        System.out.println(++counter);
        System.out.println(counter++);
        System.out.println(counter);

        System.out.println("PASS BY VALUE/REFERENCE");
        counter = 1;
        increment(counter);
        increment(counter);
        System.out.println(counter);

        Counter c = new Counter();
        c.setValue(1);
        increment(c);
        increment(c);
        System.out.println(c.getValue());
    }

    public void increment(int counter){
        ++counter;
    }

    public void increment(Counter counter){
        counter.increment();
    }

    public void defaultPrimitiveValues(){
        PrimitiveValues primitiveValues = new PrimitiveValues();
        System.out.println(primitiveValues);
    }

    public void types(){
        int i = 0;
        boolean b = true;

    }

    public void methodsAttributes() {
        System.out.println(Animal.PUB_STATIC_NAME);
        System.out.println(Animal.getStaticNameStatic());

        Animal animal = new Animal("a");
        System.out.println(animal.getStaticName());
    }
}
