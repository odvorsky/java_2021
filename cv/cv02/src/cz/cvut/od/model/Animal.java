package cz.cvut.od.model;

public class Animal {

    private String name;
    private static final String staticName = "AAAA";
    public static final String PUB_STATIC_NAME = "AAAA";

    public Animal(){

    }

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static String getStaticNameStatic() {
        return staticName;
    }

    public String getStaticName() {
        return staticName;
    }
}
