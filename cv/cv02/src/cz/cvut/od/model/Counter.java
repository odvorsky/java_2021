package cz.cvut.od.model;

    public class Counter {

    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void increment(){
        this.value++;
    }
}
