package cz.cvut.od.model;

public enum FuelType {
    DIESEL,
    GASOLINE
}
