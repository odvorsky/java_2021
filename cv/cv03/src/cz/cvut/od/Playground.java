package cz.cvut.od;

import cz.cvut.od.model.AbstractUser;
import cz.cvut.od.model.Admin;
import cz.cvut.od.model.User;

public class Playground {

    public void play(){
        autoboxingUnboxing();
        npe();
        nan();
        inheritance();
    }

    public void inheritance(){
        Admin admin = new Admin();
        admin.setUsername("AAAA");
        System.out.println(admin.getAdminUsername());
        System.out.println(admin);

        User user = new Admin("AAAA");
        if(user instanceof Admin){
            System.out.println(((Admin) user).getAdminUsername());
        }
        System.out.println(user);

        User user2 = new User("BBBB");
        //((Admin) user2).getAdminUsername();
        System.out.println(user2);

        System.out.println(user.getInfo());
        System.out.println(admin.getInfo());

        AbstractUser eshopUser = new User("AAAAA -");
        System.out.println(eshopUser.cleanUsername());

        AbstractUser eshopUser1 = new Admin(" BBBB");
        System.out.println(eshopUser1.cleanUsername());

    }

    public void nan(){
        double sqrt = Math.sqrt(-1);
        if(Double.isNaN(sqrt)){
            System.out.println("is NaN");
        }
        System.out.println(sqrt);
    }

    public void npe(){
        User user = null;
        if(user != null){
            user.getUsername();
        }
    }

    public void autoboxingUnboxing(){
        Integer i = 0;
        int mi = i;

        Boolean b = Boolean.TRUE;
        if(b){
            System.out.println("AAAA");
        }
        b = null;
        if(Boolean.TRUE.equals(b)){
            System.out.println("BBBB");
        }
    }

    public void packages(){
        Test test = new Test();
        cz.cvut.od.model.Test test1 = new cz.cvut.od.model.Test();
    }

}
