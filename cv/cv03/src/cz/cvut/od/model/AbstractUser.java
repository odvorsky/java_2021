package cz.cvut.od.model;

public abstract class AbstractUser {

    public abstract String getUsername();

    public String cleanUsername(){
        return getUsername().trim().replace("-", "");
    }
}
