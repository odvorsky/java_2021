package cz.cvut.od.model;

public class Admin extends User {

    public static String getInfo(){
        return "Admin static";
    }

    public Admin(){
        super();
        System.out.println("ADMIN");
    }

    public Admin(String username) {
        super(username);
    }

    public String getAdminUsername(){
        return getUsername() + "-ADMIN";
    }

    @Override
    public String toString() {
        return super.toString() + " ADMIN";
    }
}
