package cz.cvut.od.model;

public class User extends AbstractUser{

    private String username;

    public static String getInfo(){
        return "User static";
    }

    public User() {
        System.out.println("USER");
    }

    public User(String username) {
        this.username = username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                '}';
    }

    @Override
    public String getUsername() {
        return username;
    }
}
