package cz.cvut.od;

import cz.cvut.od.du.*;
import cz.cvut.od.model.*;

public class Playground {


    public void play(){
        du();
        jobs();
        animals();
        users();
    }

    public void arr(){
        int[] intArr = new int[10];
        intArr = new int[]{1, 2, 3};
        int[] intArrStat = {1, 2, 3, 3, 4};


        Object[] users = {new Admin("dvob1", "a"), new Duck(), new AJob()};
    }

    public void users(){
        User u1 = new User("dvob1");
        User u2 = new Admin("dvob1", "Adam");

        System.out.println(u1);
        System.out.println(u2);
        System.out.println("Users are equal: " + u1.equals(u2));
    }

    public void animals(){
        Animal cat = new Cat();
        cat.setName("A");
        printAnimalInfo(cat);

        Animal cat2 = new Cat();
        cat2.setName("A");

        if(cat.equals(cat2)){
            System.out.println("CATS are equal");
        }

        Animal duck = new Duck();
        printAnimalInfo(duck);
    }

    public void printAnimalInfo(Animal animal){
        System.out.println(animal.info());
    }

    public void jobs(){
        AbstractJob a = new AJob();
        a.process();

        AbstractJob b = new BJob();
        b.process();

        processJobs(a, b);
    }

    public void processJobs(AbstractJob... jobs){
        for (AbstractJob job: jobs){
            job.runtimePoly();
            job.process();
        }
    }

    public void du(){
        Polygon square = new Square(5);

        Polygon rect = new Rectangle(5,5);

        System.out.println(square.getArea());
        System.out.println(rect.getArea());

        Animal cat = new Cat();
        System.out.println(cat.info());
        cat.makeSound();
    }

}
