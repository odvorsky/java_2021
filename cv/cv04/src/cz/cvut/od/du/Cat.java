package cz.cvut.od.du;

public class Cat extends Animal{

    @Override
    public String info() {
        return "Kočka";
    }

    @Override
    public void makeSound() {
        System.out.println("mnau");
    }

    @Override
    public String toString() {
        return "Kočka";
    }
}
