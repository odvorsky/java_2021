package cz.cvut.od.du;

public class Duck extends Animal {

    @Override
    public String info() {
        return "Kachna " + super.info();
    }

    @Override
    public void makeSound() {
        System.out.println("quack");
    }
}
