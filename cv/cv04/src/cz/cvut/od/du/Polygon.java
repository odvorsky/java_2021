package cz.cvut.od.du;

public interface Polygon {

    float getArea();
}
