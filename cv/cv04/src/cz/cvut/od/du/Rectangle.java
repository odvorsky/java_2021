package cz.cvut.od.du;

public class Rectangle implements Polygon{

    private float a;
    private float b;

    public Rectangle(float a, float b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public float getArea() {
        return a*b;
    }
}
