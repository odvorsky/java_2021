package cz.cvut.od.du;

public class Square implements Polygon {

    private float a;

    public Square(float a) {
        this.a = a;
    }

    @Override
    public float getArea() {
        return a*a;
    }
}
