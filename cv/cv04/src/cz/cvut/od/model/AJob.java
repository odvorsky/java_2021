package cz.cvut.od.model;

public class AJob extends AbstractJob {

    @Override
    public String getName() {
        return "A";
    }

    @Override
    public void runtimePoly() {
        System.out.println("Runtime Poly on job: " + getName());
    }

    @Override
    protected void processInternal() {
        System.out.println("AA job is running...");
    }
}
