package cz.cvut.od.model;

public abstract class AbstractJob {

    public abstract String getName();
    protected abstract void processInternal();

    public void runtimePoly(){
        System.out.println("runtime poly on AbstractJob");
    }

    public void process(){
        System.out.println("starting the processing of job: " + getName());
        processInternal();
        System.out.println(String.format("execution of job: %s", getName()));
    }

}
