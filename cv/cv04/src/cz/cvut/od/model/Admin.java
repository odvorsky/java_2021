package cz.cvut.od.model;

public class Admin extends User {

    private String name;

    public Admin(String username, String name) {
        super(username);
        this.name = name;
    }

    @Override
    public String toString() {
        return "Admin{" +
                "username='" + getUsername() + '\n' +
                "name='" + name + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
