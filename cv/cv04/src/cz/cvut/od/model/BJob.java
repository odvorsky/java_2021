package cz.cvut.od.model;

public class BJob extends AbstractJob {

    @Override
    public String getName() {
        return "B";
    }

    @Override
    protected void processInternal() {
        System.out.println("Job B is running...");
    }
}
