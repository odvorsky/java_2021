package cz.cvut.od;

import cz.cvut.od.model.BirthdayPresent;
import cz.cvut.od.model.User;

import java.util.*;

public class Playground {

    public void play(){
        //list();
        //bday();
        //listsTest();
        //sets();
        //maps();
        removal();
    }

    public void removal(){
        List<String> list = new ArrayList<>(Arrays.asList("A", "B", "C", "D", "A"));

        Iterator<String> it = list.iterator();

        for(String str: list){
            list.remove(str);
        }

        while(it.hasNext()) {
            String str = it.next();
            System.out.println("removing item: " + str);
            it.remove();
        }

    }

    public void maps() {
        User userA = new User("A");
        User userB = new User("B");

        Map<String, User> users = new HashMap<>();
        users.put(userA.getUsername(), userA);
        users.put(userA.getUsername(), userB);
        for (String username : users.keySet()) {
            User user = users.get(username);
            System.out.println("key: " + username + ", " + user);
        }
        for (Map.Entry<String, User> entry : users.entrySet()) {
            System.out.println("key: " + entry.getKey() + ", " + entry.getValue());
        }
    }

    public void sets(){
        Set<String> set = new HashSet<>(List.of("A", "B", "C", "D", "E", "A"));
        for(String str: set){
            System.out.println(str);
        }
        Set<User> users = new HashSet<>(List.of(new User("A"), new User("B"), new User("A")));
        for(User str: users) {
            System.out.println(str);
        }
    }

    public void list(){
        List<String> linkedList = new LinkedList<>();
        linkedList.add("A");

        List<String> arrList = new ArrayList<>();
        arrList.add("A");
        arrList.add("A");
        arrList.add("B");
        //arrList.add(1);

        for (Object obj: arrList) {
            String sobj = (String) obj;
        }
    }

    public void bday(){
        BirthdayPresent<String> birthdayPresent = new BirthdayPresent<>("ahoj");
        System.out.println(birthdayPresent.unwrap());
    }

    public void listsTest(){
        System.out.println("ARRAYLIST WARMUP");
        for (int i = 0; i < 10; i++) {
            perfTest(new ArrayList<>());
        }
        System.out.println("LINKEDLIST WARMUP");
        for (int i = 0; i < 10; i++) {
            perfTest(new LinkedList<>());
        }
    }

    public void perfTest(List<Integer> integerList) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10000; i++) {
            integerList.add(i);
        }
        for (int i = 0; i < integerList.size(); i++) {
            Integer item = integerList.get(i);
        }
        long endTime = System.currentTimeMillis();
        for (int i = 0; i < integerList.size(); i++) {
            integerList.remove(i);
        }
        System.out.println("Perf test took: " + (endTime - startTime));
    }
}
