package cz.cvut.od.model;

public class BirthdayPresent<T> {
    private T content;

    public BirthdayPresent(T content){
        this.content = content;
    }

    public void wrap(T content){
        this.content = content;
    }

    public T unwrap(){
        return content;
    }
}
