package od;

import od.ex.FirstCheckedException;
import od.ex.FirstUncheckedException;

public class Playground {

    public void play() {
        uncheckedException(-1);
        rethrow();

        System.out.println("execution done.");
    }


    public void rethrow(){
        try {
            checkedException(25);

        } catch (FirstCheckedException e) {
            e.printStackTrace();
            throw new IllegalStateException("error", e);
        }
    }

    public void pipeCatchBlock(){
        try {
            checkedException(25);

        } catch (FirstCheckedException | RuntimeException e) {
            e.printStackTrace();
        }
    }

    public void multiCatchBlock(){
        try {
            checkedException(25);

        } catch (FirstCheckedException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkedException(int i) throws FirstCheckedException {
        if (i > 16) {
            throw new FirstCheckedException();
        }
    }

    public void uncheckedException(int i) {
        if (i < 0) {
            throw new FirstUncheckedException("user input was smaller than 0");
        }
    }
}
