package od.ex;

public class FirstUncheckedException extends RuntimeException {


    public FirstUncheckedException() {
    }

    public FirstUncheckedException(String message) {
        super(message);
    }
}
