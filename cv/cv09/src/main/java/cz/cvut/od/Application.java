package cz.cvut.od;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException {
        new Playground().play();
    }
}
