package cz.cvut.od;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;

public class Playground {

    public void play() throws IOException {
        System.out.println("AAAA");
        //this.readFromConsole();
        //this.readLargerFile();
        this.writeToFile();
    }

    public void writeToFile() throws IOException {
        List<String> strings = List.of("A", "B", "C", "D", "E");
        try(BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("new.txt"))) {
            for (String s : strings) {
                bufferedWriter.write(s);
                bufferedWriter.newLine();
            }
        }

    }

    public void readLargerFile() throws IOException {
        /*try (FileReader fileReader = new FileReader("test.txt")){
            char[] buffer = new char[8000];
            while(fileReader.read(buffer) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        try (BufferedReader bfr = new BufferedReader(new FileReader("test.txt"))) {
            String line = bfr.readLine();
            while (line != null) {
                System.out.println(line);
                line = bfr.readLine();
            }
        }
    }

    public void readFromConsole() throws IOException {
        InputStream is = System.in;
        InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
        BufferedReader bfr2 = null;

        try {
            new BufferedReader(new InputStreamReader(System.in, StandardCharsets.UTF_8));
            bfr2 = new BufferedReader(isr);
            String line = bfr2.readLine();
            while (line != null && !"EXIT".equals(line)) {
                System.out.println(line);
                line = bfr2.readLine();
            }
        } finally {
            if (bfr2 != null) {
                bfr2.close();
            }
        }


        try (BufferedReader bfr = new BufferedReader(isr)) {
            String line = bfr.readLine();
            while (line != null && !"EXIT".equals(line)) {
                System.out.println(line);
                line = bfr.readLine();
            }
        }

    }
}
