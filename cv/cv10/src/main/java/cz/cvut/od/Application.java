package cz.cvut.od;

import cz.cvut.od.model.Playground;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        new Playground().play();
    }
}
