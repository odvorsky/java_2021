package cz.cvut.od.model;

public class MyFirstRunnable implements Runnable {

    public void run() {
        System.out.println(Thread.currentThread().getName() + ": My first runnable executed.");
    }
}
