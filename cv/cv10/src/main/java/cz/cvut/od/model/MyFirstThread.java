package cz.cvut.od.model;

public class MyFirstThread extends Thread {

    @Override
    public void run() {
        try {
            Thread.sleep(5000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName() + ": My first thread executed.");
    }

}
