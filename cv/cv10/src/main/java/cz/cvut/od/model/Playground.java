package cz.cvut.od.model;

public class Playground {

    public void play() throws InterruptedException {
        System.out.println(Thread.currentThread().getName() + ": play method called.");
        Thread threadOne = threadStart();
        runnableStart();
        anonymousClassThread();
        lambdaThread();
        threadOne.join();
        System.out.println(Thread.currentThread().getName() + ": play method call finished.");
    }

    public Thread threadStart() {
        Thread thread = new MyFirstThread();
        thread.start();
        return thread;
    }

    public void anonymousClassThread() {
        new Thread() {
            @Override
            public void run() {
                System.out.println("anonymous class thread");
            }
        }.start();
    }

    public void lambdaThread() {
        new Thread(() -> System.out.println("lambda thread called")).start();
    }

    public void runnableStart() {
        new Thread(new MyFirstRunnable()).start();
    }
}
