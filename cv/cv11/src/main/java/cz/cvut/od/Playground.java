package cz.cvut.od;

import cz.cvut.od.model.Counter;
import cz.cvut.od.model.User;
import cz.cvut.od.threads.CounterThread;

public class Playground {

    public void play() throws InterruptedException {
        //this.counterSyncMethod();
        //this.counterClassLock();
        //this.twoLocksSameInstance();
        this.deadlockUserGreetings();
    }

    public void deadlockUserGreetings() throws InterruptedException {
        User adam = new User("Adam");
        User david = new User("David");

        new Thread(() -> adam.greet(david)).start();
        new Thread(() -> david.greet(adam)).start();

        System.out.println("AAAAA");
    }


    public void twoLocksSameInstance() throws InterruptedException {
        Counter counter = new Counter();

        CounterThread incrementCounterThread = new CounterThread(counter, 1000000, Counter::incrementOne);
        incrementCounterThread.start();

        CounterThread incrementCounterTwoThread = new CounterThread(counter, 1000000, Counter::incrementTwo);
        incrementCounterTwoThread.start();

        incrementCounterThread.join();
        incrementCounterTwoThread.join();

        System.out.println(counter);

    }

    public void counterClassLock() throws InterruptedException {
        Counter counter = new Counter();
        Counter counter2 = new Counter();

        CounterThread incrementCounterThread = new CounterThread(counter2, 1000000, localCounter -> localCounter.incrementOne());
        incrementCounterThread.start();

        CounterThread incrementCounterThread2 = new CounterThread(counter2, 1000000, localCounter -> localCounter.incrementOne());
        incrementCounterThread2.start();

        incrementCounterThread.join();
        incrementCounterThread2.join();
        System.out.println(counter);

    }

    public void counterSyncMethod() throws InterruptedException {
        Counter counter = new Counter();

        CounterThread incrementCounterThread = new CounterThread(counter, 1000000, localCounter -> localCounter.incrementOne());

        incrementCounterThread.start();

        CounterThread decrementCounterThread = new CounterThread(counter, 1000000, localCounter -> localCounter.decrement());
        decrementCounterThread.start();

        incrementCounterThread.join();
        decrementCounterThread.join();
        System.out.println(counter);
    }
}
