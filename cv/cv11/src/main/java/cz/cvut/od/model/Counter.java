package cz.cvut.od.model;

public class Counter {
    private int count;
    private int countTwo;

    private final Object lockOne = new Object();
    private final Object lockTwo = new Object();


    public void incrementOne() {
        synchronized (lockOne) {
            count++;
        }
    }

    public void incrementTwo() {
        synchronized (lockTwo) {
            countTwo++;
        }

    }

    public void incrementSyncBlock() {
        synchronized (this) {
            count++;
        }
    }

    public synchronized void decrement() {
        count--;
    }

    @Override
    public String toString() {
        return "Counter{" +
                "count=" + count +
                ", countTwo=" + countTwo +
                '}';
    }
}
