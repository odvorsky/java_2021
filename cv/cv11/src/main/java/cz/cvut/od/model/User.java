package cz.cvut.od.model;

public class User {
    private String name;

    public User(String name) {
        this.name = name;
    }

    public void greet(User user) {
        System.out.printf("%s greets %s%n", this.getName(), user.getName());
        user.greetBack(this);
    }

    public void greetBack(User user) {
        System.out.printf("%s greets %s back%n", this.getName(), user.getName());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
