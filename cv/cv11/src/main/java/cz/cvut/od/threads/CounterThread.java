package cz.cvut.od.threads;

import cz.cvut.od.model.Counter;

import java.util.function.Consumer;

public class CounterThread extends Thread{

    private Counter counter;
    private int times;
    private Consumer<Counter> consumer;

    public CounterThread(Counter counter, int times, Consumer<Counter> consumer){
        this.counter = counter;
        this.times = times;
        this.consumer = consumer;
    }

    @Override
    public void run() {
        for(int i = 0; i < times; i++){
            consumer.accept(counter);
        }
    }
}
