package cz.cvut.od;

import cz.cvut.od.model.State;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantLock;

public class Playground {

    private final State state = new State();
    private volatile boolean canRun;
    ExecutorService executorService = Executors.newFixedThreadPool(50);

    public void play() {
        //this.polling();
        this.waitNotify();


    }

    public void waitNotify() {
        Thread b = new Thread("B") {
            @Override
            public void run() {
                while (true) {
                    synchronized (state) {
                        try {
                            state.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (state.isCanRun()) {
                            break;
                        }
                    }
                }
                System.out.printf("Thread %s can run now. \n", Thread.currentThread().getName());
            }
        };
        b.start();

        new Thread("A") {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Setting canProceed to true.");
                synchronized (state) {
                    state.setCanRun(true);
                    state.notifyAll();
                }
                //canRun = true;
            }
        }.start();
    }

    public void polling() {

        Thread b = new Thread("B") {
            @Override
            public void run() {
                while (true) {
                    if (canRun) {
                        break;
                    }
                }
                System.out.printf("Thread %s can run now. \n", Thread.currentThread().getName());
            }
        };
        b.start();

        new Thread("A") {
            @Override
            public void run() {
                try {
                    Thread.sleep(5000L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Setting canProceed to true.");
                //state.setCanRun(true);
                canRun = true;
            }
        }.start();
    }
}
