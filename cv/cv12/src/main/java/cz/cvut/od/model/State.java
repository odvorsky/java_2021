package cz.cvut.od.model;

public class State {

    private boolean canRun;

    public boolean isCanRun() {
        return canRun;
    }

    public void setCanRun(boolean canRun) {
        this.canRun = canRun;
    }
}
