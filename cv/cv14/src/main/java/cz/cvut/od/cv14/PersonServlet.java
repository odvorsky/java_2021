package cz.cvut.od.cv14;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.od.cv14.model.Person;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@WebServlet(name = "person", value = "/person")
public class PersonServlet extends HttpServlet {

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final Map<String, Person> data = new HashMap<>();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        String id = req.getParameter("id");
        if(id != null){
            objectMapper.writeValue(resp.getWriter(), data.get(id));
        }
        objectMapper.writeValue(resp.getWriter(), data);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");

        String id = UUID.randomUUID().toString();
        Person person = objectMapper.readValue(req.getReader(), Person.class);
        person.setId(id);

        data.put(id, person);
        objectMapper.writeValue(resp.getWriter(), person);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPut(req, resp);
    }
}
