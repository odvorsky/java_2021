package cz.cvut.od.cv14.model;

public class Person {
    private String id;
    private String name;
    private String birthNumber;

    public Person() {
    }

    public Person(String name, String birthNumber) {
        this.name = name;
        this.birthNumber = birthNumber;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthNumber() {
        return birthNumber;
    }

    public void setBirthNumber(String birthNumber) {
        this.birthNumber = birthNumber;
    }
}
