Hlavním cílem je odevzdat funkční síťové piškvorky fungující v režimu klient-server.

Požadavky:
- hra bude plně funkční = lze ji hrát a vyhrát
- hrají vždy 2 klienti s UI příkazové řádky proti sobě, na server se může paralelně připojit N klientů
- veškerý stav hry drží server, klient jen podává uživateli data
- klient před připojením získá od uživatele jeho info (jméno), značku mu přidělí server sám
- pokud se klient připojí a v herní frontě na serveru nečeká žádný protivník, je třeba aby hráč čekal na připojení protivníka
- po dokončení hry se UI zeptá, jestli chce hráč hrát znovu, pokud ne, aplikace může skončit
- pro herní logiku, (zejména pak vyhodnocení výhry) budou napsány unit testy
- klienti budou logovat veškerou komunikaci do souboru přes log4j2 framework
- server bude logovat taktéž, implementace loggingu je pak ponechána na tom, jaký přístup k tvorbě serveru/komunikace využijete

Server/komunikační technologie:
- Client/ServerSocket
	- ukazovali jsme si, jak se dá přes sockety vytvořit jednoduchý server
	- dá se použít, je ovšem nutné vhodně řešit synchronizaci vláken
	- zasílání zpráv je složitější, jelikož se jedná o dvousměrnou komunikaci
	- jedná se ovšem o dobré cvičení na multithreading
- J2EE Servlety (Tomcat)
	- nad piškvorkami se dá postavit HTTP/REST API
	- synchronní komunikace, tzn. párování hráčů je nutné vyřešit (např. polling, ServerSentEvents?)
	- data v paměti serveru, pozor na sdílené zdroje - je třeba zaručit konzistenci a thread-safety
	- jednodušší komunikace
- využití SpringBootu (embedded Tomcat/Jetty)
	- tvorba stejného API jako v případě servletů, ovšem modernější a snadnější
	- pro Vás nejpřínosnější, ovšem nejnáročnější, co se konzultací/samostudia týče
	- implementačně ovšem ušetří práci