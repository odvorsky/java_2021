package cz.cvut.od.springdemoclient;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.od.springdemoclient.model.Person;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;
@SpringBootApplication
public class SpringDemoClientApplication {

	@Bean
	public RestTemplate restTemplate(){
		MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getMessageConverters()
				.add(mappingJackson2HttpMessageConverter);
		return restTemplate;
	}

	public static void main(String[] args) throws JsonProcessingException {
		SpringApplication.run(SpringDemoClientApplication.class, args);
	}
}
