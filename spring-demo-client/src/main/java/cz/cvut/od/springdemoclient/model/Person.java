package cz.cvut.od.springdemoclient.model;

public class Person {
    private Long id;
    private String name;
    private String birthNumber;

    public Person() {
    }

    public Person(String name, String birthNumber) {
        this.name = name;
        this.birthNumber = birthNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthNumber() {
        return birthNumber;
    }

    public void setBirthNumber(String birthNumber) {
        this.birthNumber = birthNumber;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthNumber='" + birthNumber + '\'' +
                '}';
    }
}
