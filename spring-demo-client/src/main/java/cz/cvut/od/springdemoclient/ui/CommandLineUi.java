package cz.cvut.od.springdemoclient.ui;

import cz.cvut.od.springdemoclient.model.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Component
public class CommandLineUi implements CommandLineRunner {

    private final RestTemplate restTemplate;
    private static final Logger log = LoggerFactory.getLogger(CommandLineUi.class);

    public CommandLineUi(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public void run(String... args) throws Exception {

        List<Person> persons = Arrays.asList(restTemplate.getForObject("http://localhost:8080/person", Person[].class));
        log.info(persons.toString());
    }
}
