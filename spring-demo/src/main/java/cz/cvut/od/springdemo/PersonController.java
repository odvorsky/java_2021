package cz.cvut.od.springdemo;

import cz.cvut.od.springdemo.model.Person;
import cz.cvut.od.springdemo.repository.PersonRepository;
import cz.cvut.od.springdemo.service.PersonService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonRepository personRepository;
    private final PersonService personService;

    private Map<String, Person> personData = new ConcurrentHashMap<>();

    public PersonController(PersonRepository personRepository, PersonService personService) {
        this.personRepository = personRepository;
        this.personService = personService;
    }

    @GetMapping("/test")
    public String test(){
        return personService.test();
    }

    @GetMapping("/{id}")
    public Person getPerson(@PathVariable Long id){
        return personRepository.findById(id).orElse(null);
    }

    @GetMapping
    public List<Person> getAll(){
        return personRepository.findAll();
    }

    @PostMapping
    public Person create(@RequestBody Person person){
        String uuid = UUID.randomUUID().toString();
        return personRepository.save(person);
    }

    @PutMapping("/{id}")
    public Person update(@PathVariable Long id, @RequestBody Person person){
    Person person1 = personRepository.findById(id).orElse(null);
    person1.setName(person.getName());
    person1.setBirthNumber(person.getBirthNumber());
    return personRepository.save(person1);
    }
}
