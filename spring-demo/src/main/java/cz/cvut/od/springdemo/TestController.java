package cz.cvut.od.springdemo;

import cz.cvut.od.springdemo.service.PersonService;
import cz.cvut.od.springdemo.service.PersonServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    private final PersonService personService;

    public TestController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/")
    public void testGet(){
        System.out.println();
    }
}
