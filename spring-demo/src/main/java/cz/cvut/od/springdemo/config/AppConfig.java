package cz.cvut.od.springdemo.config;

import cz.cvut.od.springdemo.service.FullPersonServiceImpl;
import cz.cvut.od.springdemo.service.PersonService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class AppConfig {

    @Bean
    @Profile("full")
    public PersonService personService(){
        return new FullPersonServiceImpl();
    }

}
