package cz.cvut.od.springdemo.repository;

import cz.cvut.od.springdemo.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Long> {

}
