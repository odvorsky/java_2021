package cz.cvut.od.springdemo.service;

public class FullPersonServiceImpl implements PersonService {

    @Override
    public String test() {
        return "full person service";
    }
}
