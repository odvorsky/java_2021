package cz.cvut.od.springdemo.service;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("normal")
public class PersonServiceImpl implements PersonService{

    @Override
    public String test(){
        return "normal person service";
    }

}
