- Do vhodného package vytvořte třídu Illness s vlastnostmi code, name a hadTimes (představuje kolikrát měl pacient nemoc) - použijte vhodné datové typy
- Do vhodného package vytvořte třídu Patient s vlastnostmi forename, surname, personalId (opět použijte vhodné datové typy) a illness (typ Illness)
- Vlastnost personalId nebude možné po vytvoření instance Patient změnit - vytvořte odpovídající get/set metody
- Vlastnost illness bude vznikat až v konstruktoru třídy Patient, pacient může existovat i bez nemoci
- Vytvořte metodu, která vytiskne informace o pacientovi - pokud má nemoc, tak i informace o nemoci
- Ve třídě Patient vytvořte metodu, která inkrementuje hadTimes na instanci Illness (pokud tam je)
    - POZOR!!! inkrementace proběhne právě ve třídě Patient, nechci po vás metodu increment na Illness, která ze z Patient pouze zavolá 
    (tzn. Illness obsahuje pouze vhodné get/set metody)
- V main metodě si s vlastními daty vytvořte test instance třídy Patient jak s nemocí, tak bez ní, vytiskněte informace
o obou instancích, u obou zavolejte metodu pro povýšení hadTimes a opět obě instance vytiskněte
