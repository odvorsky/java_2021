package cz.cvut.kbi;

import cz.cvut.kbi.ui.CommandLineUi;

public class Application {

    public static void main(String[] args) {
        new CommandLineUi().start();
    }
}
