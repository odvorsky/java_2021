Úloha "sequenceCalculator"
- cílem je počkat na výsledky jednotlivých výpočtů CalculatorThreadů tak, abychom dostali správný výsledek 15

Úloha "gameQueue"
- cílem je naprogramovat párování hráčů do her o 2 hráčích z herní fronty
- model pro Player i Game již máte připraven
- v momentě, kdy párovací thread (GameQueue) zjistí, že má vytvořit 2 hráčům hru, je vytvořena instance třídy Game (předání oba hráči) a hra je vytištěna
- musí v předepsaném scénáři ve třídě Application, tzn. celkem by mělo vzniknout 23 her a fronta by po doběhnutí měla být prázdná
- tzn. v application tam různé thready přistupují a registrují hráče, GameQueue musí bezpečně registrovat a co nejrychleji párovat a přidělovat hry

