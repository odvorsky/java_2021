package cz.cvut.kbi;

import cz.cvut.kbi.model.GameQueue;
import cz.cvut.kbi.model.Player;
import cz.cvut.kbi.thread.CalculatorThread;

import java.util.Random;

public class Application {

    public static void main(String[] args) throws InterruptedException {
        sequenceCalculator();
        //gameQueue();
    }

    public static void sequenceCalculator() throws InterruptedException {
        long result;
        CalculatorThread ctOne = new CalculatorThread(5, 5);
        CalculatorThread ctTwo = new CalculatorThread(2, 2);
        CalculatorThread ctThree = new CalculatorThread(1, 0);

        ctOne.start();
        ctTwo.start();
        ctThree.start();

        result = ctOne.getResult() + ctTwo.getResult() + ctThree.getResult();
        System.out.println("result is: " + result);
    }

    public static void gameQueue() throws InterruptedException {
        GameQueue gameQueue = new GameQueue();
        gameQueue.start();
        spawnPlayers(gameQueue, 27);
        spawnPlayers(gameQueue, 18);
        Thread.sleep(1000L);
        spawnPlayers(gameQueue, 1);
        System.out.println("queue size after end: " + gameQueue.getQueueSize());
    }

    private static void spawnPlayers(GameQueue gameQueue, int number) {
        new Thread(() -> {
            for (int i = 0; i < number; i++) {
                String name = "hráč_" + new Random().nextInt();
                gameQueue.registerPlayer(new Player(name));
            }
        }).start();
    }
}
