package cz.cvut.kbi.model;

import java.util.LinkedList;
import java.util.Queue;

public class GameQueue extends Thread {

    private final Queue<Player> queue = new LinkedList<>();

    public void registerPlayer(Player player) {
    }

    public int getQueueSize() {
        return queue.size();
    }

    @Override
    public void run() {
        System.out.println("Game Queue started! Waiting for players to join...");
    }
}
