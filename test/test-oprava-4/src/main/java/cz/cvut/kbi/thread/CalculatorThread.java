package cz.cvut.kbi.thread;

public class CalculatorThread extends Thread {

    private long result;
    private final long argOne;
    private final long argTwo;

    public CalculatorThread(long argOne, long argTwo){
        this.argOne = argOne;
        this.argTwo = argTwo;
    }

    public long getResult() {
        return result;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        result = argOne + argTwo;
    }
}
