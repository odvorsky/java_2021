- Do vhodného package vytvořte třídu Illness s vlastnostmi code, name a dangerLevel (použijte vhodné datové typy)
- Do vhodného package vytvořte třídu Patient s vlastnostmi forename, surname, personalId (použijte vhodné datové typy) a illness (typ Illness)
- Vlastnost personalId nebude možné po vytvoření změnit - vytvořte get/set metody
- Vlastnost illness bude vznikat až v konstruktoru třídy Patient, pacient může existovat i bez nemoci
- Vytvořte metodu, která vytiskne informace o pacientovi, pokud má nemoc, tak i informace o nemoci
- Vytvořte metodu, která zvýší dangerLevel a vrátí celou instanci nemoci
- V main metodě si s vlastními daty vytvořte test instance třídy Patient jak s nemocí, tak bez ní, vytiskněte informace
o obou instancích, povyšte dangerLevel u nemoci nemocného Patienta a vytiskněte informace o této nemoci
