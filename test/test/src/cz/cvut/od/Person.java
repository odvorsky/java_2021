package cz.cvut.od;

public class Person {

    private static final int INITIAL_AGE = 59;

    private String forename;
    private String surname;
    private String email;
    private int age;

    public Person(String forename, String surname, String email, int age) {
        this.forename = forename;
        this.surname = surname;
        this.email = email;
        this.age = age;
    }

    public void incrementAge(){
        age++;
    }

    public void incrementAgeAndPrintInfo(){
        incrementAge();
        printInfo();
    }

    public void printInfo(){
        String result = this.toString();
        if(age >= 60){
            result = result + ", duchodce.";
        }
        System.out.println(result);
    }

    public String getForename() {
        return forename;
    }

    public void setForename(String forename) {
        this.forename = forename;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "forename='" + forename + '\'' +
                ", surname='" + surname + '\'' +
                ", email='" + email + '\'' +
                ", age=" + age +
                '}';
    }

    public static void main(String[] args) {
        Person person = new Person("Adam", "Kral", "a@b.cz", INITIAL_AGE);
        person.printInfo();
        person.incrementAgeAndPrintInfo();
    }
}
