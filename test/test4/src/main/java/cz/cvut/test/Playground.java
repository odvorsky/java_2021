package cz.cvut.test;

import cz.cvut.test.model.Person;
import cz.cvut.test.model.Semaphore;

public class Playground {

    public void play() {
        //this.greetings();
        this.semaphoreCrossing();
    }


    public void greetings() {
        Person john = new Person("John");
        Person peter = new Person("Peter");
        new Thread(() -> john.greet(peter)).start();

        new Thread(() -> peter.greet(john)).start();
    }

    public void semaphoreCrossing() {
        Semaphore semaphore = new Semaphore();
        Person john = new Person("John");
        semaphore.start();
        new Thread(() -> john.crossStreet(semaphore))
                .start();
    }
}
