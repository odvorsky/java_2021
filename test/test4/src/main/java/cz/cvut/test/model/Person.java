package cz.cvut.test.model;

public class Person {

    private final String name;

    public Person(String name){
        this.name = name;
    }

    public void greet(Person person){
        synchronized (Person.class){
            System.out.printf("%s greets %s\n", this.name, person.getName());
            person.greetBack(this);
        }
    }

    public void greetBack(Person person){
        synchronized (Person.class){
            System.out.printf("%s greets %s back\n", this.name, person.getName());
        }
    }

    public void crossStreet(Semaphore semaphore){
        synchronized (semaphore){
            while(!SemaphoreColor.GREEN.equals(semaphore.getCurrentColor())){
                System.out.println("Cannot cross the street, semaphore color is: " + semaphore.getCurrentColor());
                try {
                    semaphore.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Successfully crossed the street!");
        }
    }

    public String getName() {
        return name;
    }
}
