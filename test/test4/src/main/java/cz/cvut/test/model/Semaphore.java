package cz.cvut.test.model;

public class Semaphore extends Thread {
    private SemaphoreColor currentColor = SemaphoreColor.RED;

    @Override
    public void run() {
        try {
            int i = 0;
            while(i < 4){
                Thread.sleep(5000L);
                changeColor();
                i++;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public SemaphoreColor getCurrentColor() {
        return currentColor;
    }

    public synchronized void changeColor(){
        if(SemaphoreColor.RED.equals(currentColor)){
            this.currentColor = SemaphoreColor.GREEN;
        }
        else {
            this.currentColor = SemaphoreColor.RED;
        }
        System.out.println("Semaphore color switched to: " + this.currentColor.name());
        this.notifyAll();
    }
}
