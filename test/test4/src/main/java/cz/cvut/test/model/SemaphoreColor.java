package cz.cvut.test.model;

public enum SemaphoreColor {
    RED,
    GREEN
}
