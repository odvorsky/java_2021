package cz.cvut.od;

import cz.cvut.od.mode.TicTacToeClient;
import cz.cvut.od.mode.TicTacToeServer;

import java.io.IOException;

public class Application {

    public static final String SERVER_MODE = "SERVER";

    public static void main(String[] args) throws IOException {
        if(args.length > 0 && SERVER_MODE.equals(args[0])){
            new TicTacToeServer().start();
        }
        else{
            new TicTacToeClient().start();
        }

    }
}
