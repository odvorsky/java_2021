package cz.cvut.od.ex;

public class DeskSizeInvalidException extends Exception{

    public DeskSizeInvalidException(String message) {
        super(message);
    }
}
