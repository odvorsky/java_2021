package cz.cvut.od.ex;

public class InvalidMoveException extends Exception {

    public InvalidMoveException(String message) {
        super(message);
    }
}
