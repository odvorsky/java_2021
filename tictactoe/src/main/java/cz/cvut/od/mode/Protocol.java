package cz.cvut.od.mode;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Protocol {

    public static final String CONNECTION_SUCCESSFUL = "CONNECTION_SUCCESSFUL";
    public static final String PLAYER_RECEIVED = "PLAYER:";

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
}
