package cz.cvut.od.mode;

import cz.cvut.od.model.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class TicTacToeClient {

    public void start() throws IOException {
        Socket socket = new Socket("localhost", 8888);

        try (BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
             PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
             Scanner scanner = new Scanner(System.in, StandardCharsets.UTF_8)) {
            while (true) {
                String line = in.readLine();
                if (Protocol.CONNECTION_SUCCESSFUL.equals(line)) {
                    System.out.println("Zadejte username hráče: ");
                    String username = scanner.nextLine();
                    System.out.println("Zadejte znak hráče: ");
                    String mark = scanner.nextLine();
                    out.println(Protocol.PLAYER_RECEIVED + Protocol.OBJECT_MAPPER.writeValueAsString(new Player(username, mark)));
                }
            }

        }

    }
}
