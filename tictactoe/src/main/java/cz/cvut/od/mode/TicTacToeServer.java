package cz.cvut.od.mode;

import cz.cvut.od.thread.ServerThread;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TicTacToeServer {

    public void start() throws IOException {
        ServerSocket serverSocket = new ServerSocket(8888);
        while(true){
            Socket clientSocket = serverSocket.accept();
            new ServerThread(clientSocket).start();
        }
    }
}
