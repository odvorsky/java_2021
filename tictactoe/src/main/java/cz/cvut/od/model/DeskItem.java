package cz.cvut.od.model;

public class DeskItem {
    private Player player;

    public String getMark(){
        if(this.player == null){
            return "E";
        }
        return player.getMark();
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "[" + this.getMark() + "]";
    }
}
