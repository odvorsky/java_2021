package cz.cvut.od.model;

import cz.cvut.od.ex.DeskSizeInvalidException;
import cz.cvut.od.ex.InvalidMoveException;

import java.util.ArrayList;
import java.util.List;

public class Game {

    private static final int MAX_DESK_SIZE = 9;
    private static final int MIN_DESK_SIZE = 3;

    private int deskSize;
    private Player playerOne;
    private Player playerTwo;

    private List<List<DeskItem>> desk = new ArrayList<>();

    protected Game() {
    }

    public Game(int deskSize, Player playerOne, Player playerTwo) throws DeskSizeInvalidException {
        this.deskSize = deskSize;
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        generateDesk(deskSize);
    }

    public void generateDesk(int deskSize) throws DeskSizeInvalidException {
        if (deskSize > MAX_DESK_SIZE || deskSize < MIN_DESK_SIZE) {
            throw new DeskSizeInvalidException("desk size...");
        }

        for (int i = 0; i < deskSize; i++) {
            List<DeskItem> deskItems = new ArrayList<>();
            for (int j = 0; j < deskSize; j++) {
                deskItems.add(new DeskItem());
            }
            desk.add(deskItems);
        }
    }

    public void move(int x, int y) throws InvalidMoveException {
        int maxIdx = MAX_DESK_SIZE + 1;
        if(x < 1 || y < 1 || x >= maxIdx || y >= maxIdx) {
            throw new InvalidMoveException("Move index is out of range.");
        }
        Player playerOnMove = this.getPlayerOnMove();
        DeskItem deskItem = desk.get(y - 1).get(x - 1);
        if(deskItem.getPlayer() != null){
            throw new InvalidMoveException("DeskItem already marked by another player.");
        }
        deskItem.setPlayer(playerOnMove);
        this.markPlayerMoveFinished();
    }

    public Player getPlayerOnMove() {
        return playerOne.isOnMove() ? playerOne : playerTwo;
    }

    public void markPlayerMoveFinished() {
        Player playerOnMove = this.getPlayerOnMove();
        playerOnMove.setOnMove(false);
        if (playerOne.equals(playerOnMove)) {
            playerTwo.setOnMove(true);
        } else {
            playerOne.setOnMove(true);
        }
    }

    public void printDesk() {
        for (List<DeskItem> deskItems : desk) {
            for (DeskItem deskItem : deskItems) {
                System.out.print(" " + deskItem);
            }
            System.out.print("\n");
        }
    }

    public int getDeskSize() {
        return deskSize;
    }

    public Player getPlayerOne() {
        return playerOne;
    }

    public Player getPlayerTwo() {
        return playerTwo;
    }

    public List<List<DeskItem>> getDesk() {
        return desk;
    }
}
