package cz.cvut.od.model;

public class Player {
    private String username;
    private String mark;
    private boolean onMove;

    public Player(String username, String mark) {
        this.username = username;
        this.mark = mark;
    }

    public Player() {
    }

    public boolean isOnMove() {
        return onMove;
    }

    public void setOnMove(boolean onMove) {
        this.onMove = onMove;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }
}
