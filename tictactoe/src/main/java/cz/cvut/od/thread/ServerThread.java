package cz.cvut.od.thread;

import cz.cvut.od.mode.Protocol;
import cz.cvut.od.model.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServerThread extends Thread{

    public Socket socket;
    public Player player;

    public ServerThread(Socket socket){
        this.socket = socket;
    }

    public void getPlayer(PrintWriter out, BufferedReader in) throws IOException {
        do {
            String line = in.readLine();
            if(line.startsWith(Protocol.PLAYER_RECEIVED)){
                String playerJson =  line.replaceAll(Protocol.PLAYER_RECEIVED, "");
                player = Protocol.OBJECT_MAPPER.readValue(playerJson, Player.class);
                //TODO server side validace?
            }
        }while (player == null);
    }

    @Override
    public void run() {
        try(PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()))){
            out.println(Protocol.CONNECTION_SUCCESSFUL);
            this.getPlayer(out, in);
            System.out.println("GOT PLAYER");
        }
        catch (IOException e){
            System.out.println(e.getMessage());
        }
    }
}
