package cz.cvut.od.ui;

import cz.cvut.od.ex.DeskSizeInvalidException;
import cz.cvut.od.model.Game;
import cz.cvut.od.model.Player;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class CommandLineUi {

    public static final String CMD_LINE_PREFIX = "> ";

    public void start() throws DeskSizeInvalidException {
        System.out.println("Zadejte velikost hrací desky: ");
        System.out.print(CMD_LINE_PREFIX);
        Scanner scanner = new Scanner(System.in, StandardCharsets.UTF_8);
        while (scanner.hasNext()){
            int deskSize = scanner.nextInt();
            Game game = new Game(deskSize, new Player("A", "x"), new Player("B", "o"));
            game.printDesk();
            System.out.print(CMD_LINE_PREFIX);
        }
    }
}
