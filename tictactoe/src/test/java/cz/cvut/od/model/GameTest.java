package cz.cvut.od.model;

import cz.cvut.od.ex.DeskSizeInvalidException;
import cz.cvut.od.ex.InvalidMoveException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class GameTest {

    @Test
    public void generateDesk_deskSizeGreaterThanLimit_throwDeskSizeInvalidException() {
        Game game = new Game();

        DeskSizeInvalidException exception = Assertions.assertThrows(DeskSizeInvalidException.class,
                () -> game.generateDesk(10));

        Assertions.assertEquals("desk size...", exception.getMessage());
    }

    @Test
    public void generateDesk_deskSizeInLimit_generateDesk() throws DeskSizeInvalidException {
        Game game = new Game();
        game.generateDesk(4);

        Assertions.assertEquals(4, game.getDesk().size());
        for (int i = 0; i < game.getDesk().size(); i++) {
            List<DeskItem> deskItems = game.getDesk().get(i);
            Assertions.assertEquals(4, deskItems.size());
            for (DeskItem deskItem : deskItems) {
                Assertions.assertNotNull(deskItem);
            }
        }

    }

    @Test
    public void move_xLessThan1_throwInvalidMoveException() throws DeskSizeInvalidException {
        Game game = new Game(5, new Player("user1", "o"), new Player("user2", "x"));

        InvalidMoveException invalidMoveException =
                Assertions.assertThrows(InvalidMoveException.class, () -> game.move(0, 2));
        Assertions.assertEquals("Move index is out of range.", invalidMoveException.getMessage());
    }

    @Test
    public void move_xMoreThan9_throwInvalidMoveException() throws DeskSizeInvalidException {
        Game game = new Game(5, new Player("user1", "o"), new Player("user2", "x"));

        InvalidMoveException invalidMoveException =
                Assertions.assertThrows(InvalidMoveException.class, () -> game.move(10, 2));
        Assertions.assertEquals("Move index is out of range.", invalidMoveException.getMessage());
    }

    @Test
    public void move_yMoreThan9_throwInvalidMoveException() throws DeskSizeInvalidException {
        Game game = new Game(5, new Player("user1", "o"), new Player("user2", "x"));

        InvalidMoveException invalidMoveException =
                Assertions.assertThrows(InvalidMoveException.class, () -> game.move(2, 10));
        Assertions.assertEquals("Move index is out of range.", invalidMoveException.getMessage());
    }

    @Test
    public void move_yLessThan1_throwInvalidMoveException() throws DeskSizeInvalidException {
        Game game = new Game(5, new Player("user1", "o"), new Player("user2", "x"));

        InvalidMoveException invalidMoveException =
                Assertions.assertThrows(InvalidMoveException.class, () -> game.move(2, 0));
        Assertions.assertEquals("Move index is out of range.", invalidMoveException.getMessage());
    }

    @Test
    public void move_validRangeMoveOnEmptyDeskItem_makeMove() throws DeskSizeInvalidException, InvalidMoveException {
        Game game = new Game(5, new Player("user1", "o"), new Player("user2", "x"));

        Player playerOnMove = game.getPlayerOnMove();
        game.move(2, 3);

        DeskItem deskItem = game.getDesk().get(2).get(1);
        Assertions.assertNotNull(deskItem.getPlayer());
        Assertions.assertEquals(playerOnMove, deskItem.getPlayer());
        Assertions.assertNotEquals(playerOnMove, game.getPlayerOnMove());
    }

    @Test
    public void move_validRangeMoveOnFilledDeskItem_throwInvalidMoveException() throws DeskSizeInvalidException, InvalidMoveException {
        Game game = new Game(5, new Player("user1", "o"), new Player("user2", "x"));

        game.move(2, 3);
        InvalidMoveException invalidMoveException =
                Assertions.assertThrows(InvalidMoveException.class, () -> game.move(2, 3));

        Assertions.assertEquals("DeskItem already marked by another player.", invalidMoveException.getMessage());

    }
}
